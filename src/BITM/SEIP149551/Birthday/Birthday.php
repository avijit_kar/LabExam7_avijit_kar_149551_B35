<?php

namespace App\Birthday;

use App\Message\Message;
use App\Model\Database as DB;

use App\Utility\Utility;
use PDO;


class Birthday extends DB
{
    public $id;
    public $name;
    public $date;

    public function __construct()
    {

        parent::__construct();

    }

    public function index()
    {

    }


    public function setData($postVariableData=NULL)
    {
        if(array_key_exists('id',$postVariableData))
        {
            $this->id=$postVariableData['$postVariableData'];
        }
        if(array_key_exists('name',$postVariableData))
        {
            $this->name=$postVariableData['name'];
        }
        if(array_key_exists('date',$postVariableData))
        {
            $this->date=$postVariableData['date'];
        }
    }
    public  function  store()
    {
        $arrData=array($this->name,$this->date);
        $sql="Insert INTO birthday(name,date) VALUE (?,?)";
        //var_dump($sql);

        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);
        if($result)
            Message::setMessage("Success! Data has been inserted successfully:)");
        else
            Message::setMessage("Failed !Data has been inserted successfully:(");
        Utility::redirect('create.php');
    }//end of store method

}// end of BookTitle class